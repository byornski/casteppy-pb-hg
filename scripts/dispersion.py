"""
Parse a *.phonon or *.bands CASTEP output file for electronic/vibrational
frequency data and save or display a matplotlib plot of the electronic
or vibrational band structure or dispersion.
"""

import argparse
import os
from casteppy.data.bands import BandsData
from casteppy.data.phonon import PhononData
from casteppy.calculate.dispersion import reorder_freqs
from casteppy.plot.dispersion import output_grace, plot_dispersion


def main():
    args = parse_arguments()
    # If neither -up nor -down specified, plot both
    if not args.up and not args.down:
        args.up = True
        args.down = True

    # Read data
    path, file = os.path.split(args.filename)
    seedname = file[:file.rfind('.')]
    if file.endswith('.bands'):
        data = BandsData(seedname, path)
    else:
        data = PhononData(seedname, path, read_eigenvecs=args.reorder, read_ir=False)

    #Crop some out if requested
    if args.nb:
        data.freqs = data.freqs[:,:args.nb+1]

    #Do the unit conversion    
    data.convert_e_units(args.units)

    # Reorder frequencies if requested
    if args.reorder:
        reorder_freqs(data)

    # Plot
    if args.grace:
        output_grace(data, seedname, up=args.up, down=args.down)
    else:
        print("Plotting")
        fig = plot_dispersion(data, args.filename, btol=args.btol, up=args.up, down=args.down)
        if fig is not None:
            import matplotlib.pyplot as plt

            if args.yb:
                try:
                    y_min = min(plt.gca().lines[0].get_ydata()).m - 1.0
                    y_max = max(plt.gca().lines[args.yb+1].get_ydata()).m + 1.0
                except AttributeError:
                    y_min = min(plt.gca().lines[0].get_ydata()) - 1.0
                    y_max = max(plt.gca().lines[args.yb+1].get_ydata()) + 1.0

                    
                plt.gca().set_ylim(y_min,y_max)
            
            if args.expt:
                #Read the points from the .dat file
                expt_data = read_dat_file(args.expt)

                #Crop bands if requested
                if args.nb:
                    expt_data = expt_data[:args.nb+1]

                #Plot the actual data   
                for xy_set in expt_data:
                    x_data,y_data = zip(*xy_set)
                    plt.plot(x_data, y_data, 'kx')
                        
            # Save or show Matplotlib figure
            if args.s:
                plt.savefig(args.s)
                
            elif args.dat:
                with open(args.dat,'w') as datfile:
                    for band in plt.gca().lines[:-1]:
                        datfile.write("\n".join("{} {}".format(*xy) for xy in band.get_xydata())+'\n\n')
            else:
                plt.show()

def read_dat_file(dat_filename):
    '''Reads a datfile and returns an array of x,y values for each set'''
    
    #First read in the file data and split by double newline
    with open(dat_filename) as datfile:
        xydata = datfile.read().split('\n\n')
        
    def split_and_parse(line):
        '''Splits a line by spaces and returns each value as a float'''
        return list(map(float,line.split()))

    def parse_set(line_block):
        '''Splits a block of x y data by newlines and then parses them to floats'''
        return list(map(split_and_parse,line_block.splitlines()))

    #Return the parsed sets unless they are empty
    return [x for x in [parse_set(lb) for lb in xydata if not lb.isspace()] if x]

def parse_arguments():
    parser = argparse.ArgumentParser(
        description="""Extract phonon or bandstructure data from a .phonon or
                       .bands file and plot the band structure with
                       matplotlib""")
    parser.add_argument(
        'filename',
        help="""The .phonon or .bands file to extract the data from""")
    parser.add_argument(
        '-units',
        default='eV',
        help="""Convert frequencies to specified units for plotting (e.g
                1/cm, Ry). Default: eV""")
    parser.add_argument(
        '-yb',
        metavar='band',
        type=int,
        help='Limit the display range to 0:band')
    parser.add_argument(
        '-expt',
        metavar='in.dat',
        help="Add experimental data points")
    parser.add_argument(
        '-dat',
        metavar='out.dat',
        help='Save points to a .dat file'
    )
    parser.add_argument(
        '-s',
        metavar='outfile',
        nargs='?',
        help='Save resulting plot to a file')
    parser.add_argument(
        '-nb',
        metavar='num_bands',
        type=int,
        nargs='?',
        help='Limits the number of plotted bands'
        )
    parser.add_argument(
        '-grace',
        action='store_true',
        help='Output a .agr Grace file')

    spin_group = parser.add_mutually_exclusive_group()
    spin_group.add_argument(
        '-up',
        action='store_true',
        help='Extract and plot only spin up from *.bands')
    spin_group.add_argument(
        '-down',
        action='store_true',
        help='Extract and plot only spin down from *.bands')

    disp_group = parser.add_argument_group(
        'Dispersion arguments',
        'Arguments specific to plotting the band structure')
    disp_group.add_argument(
        '-reorder',
        action='store_true',
        help="""Try to determine branch crossings from eigenvectors and
                rearrange frequencies accordingly (only applicable if
                using a .phonon file)""")
    disp_group.add_argument(
        '-btol',
        default=10.0,
        type=float,
        help="""The tolerance for plotting sections of reciprocal space on
                different subplots, as a fraction of the median distance
                between q-points. Default: 10.0""")

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
